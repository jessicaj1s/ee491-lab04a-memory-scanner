# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall

TARGET = memoryScanner

all: $(TARGET)

memoryScanner: memoryScanner.c
	$(CC) $(CFLAGS) -o $(TARGET) memoryScanner.c

clean:
	rm $(TARGET)

test:
	cat /proc/self/maps > file.txt
	./memoryScanner file.txt

