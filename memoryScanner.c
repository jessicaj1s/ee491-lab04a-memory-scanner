///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04a - Memory Scanner
///
/// @file wc.c
/// @version 1.0
///
/// @author Jessica Jones <jjones2@hawaii.edu>
/// @brief  Lab 04a - Memory Scanner - EE 491F - Spr 2021
/// @date   9_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void info();

int main(int argc, char * argv[]) {

  //Command line parameter processing
  if (argc != 2) {
    info();
    exit(EXIT_FAILURE);
  }

  if ( argc == 2 ) {
    FILE * file;
    int numOfChar = 0;
    int numOfLines = 0;
    char chCurrent = '\0';
    char address1[12];
    char address2[12];
    char permissions[4]; 

    //File management logic
    // Open file in read only mode
    // since we are controlling the input (in the makefile) this should run no problem
    file = fopen(argv[1], "r");

    //Check if file opened successfully, if not exit
    if ( file == NULL ) {
      fprintf(stdout, "%s: Can't open [%s]\r\n", argv[0], argv[1]);
      exit(EXIT_FAILURE);
    }

    //Word counting logic
    // If file opened successfully, then write the string to file

    //formatting:
    printf("%d:\t", numOfLines);

    while ( ( chCurrent = fgetc(file) ) != EOF ) {

      //line ends in \r or \n
      if ( chCurrent == '\r' || chCurrent == '\n' ) {
        numOfLines++;
        printf("\r\n%d:\t", numOfLines);
        numOfChar = 0;
      }

      //avoid double counding \r\n 
      //if ( chPrevious == '\r' && chCurrent == '\n' ) {
      //  numOfLines--;
      //}

      if ( ( (!iscntrl(chCurrent)) && !isspace(chCurrent) && isprint(chCurrent) )  || ( (unsigned char)chCurrent >= 0xC0 ) ) {
		numOfChar++;

      //first 12 char are the first address
      if ( numOfChar <= 12 ) {
          address1[ (numOfChar-1) ] = chCurrent;
          if ( numOfChar == 12 ) {
            printf("%.12s - ", address1);
          }
      }
      
      //chars 14-25 are the second address
      if ( numOfChar >=14 && numOfChar <= 25 ) {
         address2[ (numOfChar-14) ] = chCurrent;
         if ( numOfChar == 25) {
           printf("%.12s ", address2); 
         }
      }

      //char 26-29 are the permissions 
      if ( numOfChar >= 26 && numOfChar <=29 ) {
            permissions[ (numOfChar - 26) ] = chCurrent; 
            if ( numOfChar == 29){
            printf("%.4s", permissions); 
            }
         }
      }
    }
    //Presentation logic — printing the results out		 

    fclose(file);
	
    return 0;
  }
}

void info() {
  fprintf( stdout, "Usage:  wc FILE\r\n" );
}

